package de.awacademy.blog.message;

import javax.validation.constraints.Size;

public class MessageDTO {

    private long id;

    private String title;
    @Size(min = 1, max = 500)
    private String text;

    public MessageDTO(String title, String text) {
        this.title = title;
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
