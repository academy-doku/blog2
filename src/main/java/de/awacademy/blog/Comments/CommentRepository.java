package de.awacademy.blog.Comments;

import de.awacademy.blog.message.Message;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface CommentRepository extends CrudRepository<Comment, Long> {


    List<Comment> findAllByMessage(Message message);

    List<Comment> findAllByOrderByPostedAtDesc();

    Optional<Comment> findById(long id);
}
