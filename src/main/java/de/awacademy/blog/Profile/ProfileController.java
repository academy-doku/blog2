package de.awacademy.blog.Profile;

import de.awacademy.blog.User.User;
import de.awacademy.blog.User.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class ProfileController {

    private UserRepository userRepository;


    @Autowired
    public ProfileController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("/profile/{username}")
    public String profile(@PathVariable String username, Model model, @ModelAttribute("sessionUser") User sessionUser) {
        User user = userRepository.findByUsername(username).get();

        if (sessionUser != null && sessionUser.getAdmin() == 1) {

            model.addAttribute("user", user);


            return "profile";

        }
        return "redirect:/";


    }


    @PostMapping("/profile/{username}/admin")
    public String setAdmin(@PathVariable String username, Model model, @ModelAttribute("sessionUser") User sessionUser) {

        User user = userRepository.findByUsername(username).get();

        if (sessionUser != null && sessionUser.getAdmin() == 1) {

            user.setAdmin(1);

            userRepository.save(user);

            return "redirect:/";


        }
        return "profile";
    }

    @PostMapping("/profile/{username}/moderator")
    public String setModerator(@PathVariable String username, Model model, @ModelAttribute("sessionUser") User sessionUser) {

        User user = userRepository.findByUsername(username).get();

        if (sessionUser != null && sessionUser.getAdmin() == 1) {
            user.setAdmin(2);

            userRepository.save(user);

            return "redirect:/";


        }
        return "profile";
    }
}
