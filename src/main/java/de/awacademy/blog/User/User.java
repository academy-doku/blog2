package de.awacademy.blog.User;

import de.awacademy.blog.Comments.Comment;
import de.awacademy.blog.message.Message;

import javax.persistence.*;
import java.util.List;

@Entity
public class User {

    @Id
    @GeneratedValue
    private long id;

    private String username;
    private String password;
    private int admin;

    @OneToMany(mappedBy = "user")
    private List<Message> messages;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Comment> comments;

    public User() {
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
        this.admin=0;
    }

    public String getUsername() {
        return username;
    }

    public long getId() {
        return id;
    }

    public int getAdmin() {
        return admin;
    }

    public void setAdmin(int admin) {
        this.admin = admin;
    }

    public List<Comment> getComments() {
        return comments;
    }

}
